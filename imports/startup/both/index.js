import '../../api/users'
import { Products } from '../../api/products'
import { Orders } from '../../api/orders'
import { Customers } from '../../api/customers'

import Tabular from 'meteor/aldeed:tabular';

const TabularTables = {};

Meteor.isClient && Template.registerHelper('TabularTables', TabularTables);

TabularTables.ProductList = new Tabular.Table({
    name: "ProductList",
    collection: Products,
    pub: "tabular_ProductList",
    extraFields: ['creatorId'],
    columns: [
        { data: "_id", title: "ID" },
        { data: "name", title: "Name" },
        { data: "creator()", title: "Creator" },
        {
            tmpl: Meteor.isClient && Template.productActions
        }
    ],
    responsive: true,
    autoWidth: false,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
});

TabularTables.OrderList = new Tabular.Table({
    name: "OrderList",
    collection: Orders,
    pub: "tabular_OrderList",
    extraFields: ['customerId', 'creatorId'],
    columns: [
        { data: "_id", title: "ID" },
        { data: "customer()", title: "Customer" },
        { data: "creator()", title: "Creator" },
        {
            tmpl: Meteor.isClient && Template.orderActions
        }
    ],
    responsive: true,
    autoWidth: false,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
});

TabularTables.CustomerList = new Tabular.Table({
    name: "CustomerList",
    collection: Customers,
    pub: "tabular_CustomerList",
    extraFields: ['addedById'],
    columns: [
        { data: "_id", title: "ID" },
        { data: "firstName", title: "First Name" },
        { data: "lastName", title: "Last Name" },
        { data: "creator()", title: "Creator" },
        {
            tmpl: Meteor.isClient && Template.customerActions
        }
    ],
    responsive: true,
    autoWidth: false,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
});

TabularTables.UserList = new Tabular.Table({
    name: "UserList",
    collection: Meteor.users,
    pub: "tabular_UserList",
    columns: [
        { data: "_id", title: "ID" },
        { data: "username", title: "Username" },
        { data: "profile.firstName", title: "First Name" },
        { data: "profile.lastName", title: "Last Name" },
        {
            tmpl: Meteor.isClient && Template.userActions
        }
    ],
    responsive: true,
    autoWidth: false,
    "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]]
});