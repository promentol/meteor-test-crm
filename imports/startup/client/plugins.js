import 'bootstrap'
import 'metismenu'

require( 'datatables.net' )( window, $ );
require( 'datatables.net-bs' )( window, $ );
//require( 'datatables.net-bs/css/dataTables.bootstrap.css' )

require( 'datatables.net-buttons' )( window, $ );
require( 'datatables.net-buttons/js/buttons.html5.js' )(window, $ ); 
require( 'datatables.net-buttons/js/buttons.print.js' )(window, $ );
require( 'datatables.net-buttons-bs' )( window, $ );
//require( 'datatables.net-buttons-bs/css/buttons.bootstrap.css' )

require( 'datatables.net-responsive' )( window, $ );
require( 'datatables.net-responsive-bs' )(window, $ );
//require( 'datatables.net-responsive-bs/css/responsive.bootstrap.css' )


require( 'cropper' )
require( 'cropper/dist/cropper.css' )


require( 'select2/dist/js/select2' ) ( window, $ )

import 'select2/dist/css/select2.css';
