import { FlowRouter } from 'meteor/kadira:flow-router'
import { BlazeLayout } from 'meteor/kadira:blaze-layout'

import '../../ui/views/layouts/blank'
import '../../ui/views/layouts/main'
import '../../ui/views/layouts/not-found'

import '../../ui/views/common/ibox-tools'
import '../../ui/views/common/footer'
import '../../ui/views/common/navigation'
import '../../ui/views/common/page-heading'
import '../../ui/views/common/top-navbar'
//import '../../ui/views/common/top-navigation'

import '../../ui/views/pages/auth/signIn'

import '../../ui/views/pages/products/products'
import '../../ui/views/pages/products/productForm'

import '../../ui/views/pages/orders/orders'
import '../../ui/views/pages/orders/orderForm'

import '../../ui/views/pages/customers/customers'
import '../../ui/views/pages/customers/customerForm'

import '../../ui/views/pages/users/users'
import '../../ui/views/pages/users/userForm'

function checkRegistered (ctx, redirect) {
    if (!Meteor.userId()) {
        redirect('/signIn')
    }
}

FlowRouter.route("/", {
    triggersEnter: [checkRegistered],
    action: function () {
        FlowRouter.go('/home');
    }
})

FlowRouter.route('/home', {
    triggersEnter: [checkRegistered],
    action: function () {
        if (Roles.userIsInRole(Meteor.userId(), ['products'])){
            FlowRouter.go('/products');
        } else if (Roles.userIsInRole(Meteor.userId(), ['orders'])) {
            FlowRouter.go('/orders');
        } else if (Roles.userIsInRole(Meteor.userId(), ['customers'])) {
            FlowRouter.go('/customers');
        } else if (Roles.userIsInRole(Meteor.userId(), ['moderator'])) {
            FlowRouter.go('/users');
        } else {
            FlowRouter.go('/products');
        }
    }
});

FlowRouter.route('/products', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", { content: "products" });
    }
});

FlowRouter.route('/products/add', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "productForm",
            modeEdit: false,
            title: 'Add Product'
        });
    }
})

FlowRouter.route('/products/edit/:id', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "productForm",
            modeEdit: true,
            title: 'Edit Product'
        });
    }
})

FlowRouter.route('/orders', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", { content: "orders" });
    }
});

FlowRouter.route('/orders/add', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "orderForm",
            modeEdit: false,
            title: 'Add Order'
        });
    }
});

FlowRouter.route('/orders/edit/:id', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "orderForm",
            modeEdit: true,
            title: 'Edit Order'
        });
    }
});

FlowRouter.route('/customers', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", { content: "customers" });
    }
});

FlowRouter.route('/customers/add', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "customerForm",
            modeEdit: false,
            title: 'Add Customer'
        });
    }
});

FlowRouter.route('/customers/edit/:id', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "customerForm",
            modeEdit: true,
            title: 'Edit Customer'
        });
    }
});

FlowRouter.route('/users', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "users"
        });
    }
})

FlowRouter.route('/users/add', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "userForm",
            modeEdit: false,
            title: 'Add User'
        });
    }
});

FlowRouter.route('/users/edit/:id', {
    triggersEnter: [checkRegistered],
    action: function () {
        BlazeLayout.render("mainLayout", {
            content: "userForm",
            modeEdit: true,
            title: 'Edit Customer'
        });
    }
});

FlowRouter.route('/signIn', {
    action: function () {
        BlazeLayout.render("blankLayout", { content: "signIn" });
    }
})

FlowRouter.notFound = {
    action: function() {
    	BlazeLayout.render("mainLayout", {content: "notFound"});
    }
};
