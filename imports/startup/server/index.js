import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'
import '../../api/publications'

if (Meteor.users.find().count() === 0) {
    const id = Accounts.createUser({
        username: process.env.ADMIN_USERNAME,
        email: process.env.ADMIN_EMAIL,
        password: process.env.ADMIN_PASSWORD,
        profile: {
            firstName: process.env.ADMIN_FIRST_NAME,
            lastName: process.env.ADMIN_LAST_NAME
        }
    });
    Roles.addUsersToRoles(id, ['admin', 'moderator', 'products', 'orders', 'customers']);

}
