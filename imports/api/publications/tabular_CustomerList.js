import { Customers } from '../customers'

Meteor.publishComposite("tabular_CustomerList", function (tableName, ids, fields) {
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));

    console.log(tableName, ids, fields)

    this.unblock();

    return {
        find: function () {
            this.unblock();
            return Customers.find({
                _id: { $in: ids }
            },
                {
                    fields: fields
                });
        },
        children: [
            {
                find: function (customer) {
                    this.unblock();
                    return Meteor.users.find({
                        _id: customer.addedById
                    }, {
                        limit: 1,
                        fields: { profile: 1 },
                        sort: { _id: 1 }
                    });
                }
            }
        ]
    };
});
