import { Products } from '../products'

Meteor.publishComposite("tabular_ProductList", function (tableName, ids, fields) {
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));
    
    this.unblock();

    return {
        find: function () {
            this.unblock();
            return Products.find({
                _id: { $in: ids }
            },
                {
                    fields: fields
                });
        },
        children: [
            {
                find: function (product) {
                    this.unblock();
                    return Meteor.users.find({
                        _id: product.creatorId
                    }, {
                            limit: 1,
                            fields: { profile: 1 },
                            sort: { _id: 1 }
                        });
                }
            }
        ]
    };
});
