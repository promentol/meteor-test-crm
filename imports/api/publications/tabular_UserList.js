Meteor.publish('tabular_UserList', function (tableName, ids, fields) {
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));


    return Meteor.users.find({
        _id: { $in: ids }
    },
    {
        fields: fields
    });
})