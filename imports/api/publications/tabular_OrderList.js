import { Orders } from '../orders'
//import { Products } from '../products'
import { Customers } from '../customers'

Meteor.publishComposite("tabular_OrderList", function (tableName, ids, fields) {
    check(tableName, String);
    check(ids, Array);
    check(fields, Match.Optional(Object));

    this.unblock();

    return {
        find: function () {
            this.unblock();
            return Orders.find({
                _id: { $in: ids }
            },
            {
                fields: fields
            });
        },
        children: [
            {
                find: function (order) {
                    this.unblock();
                    return Meteor.users.find({
                        _id: order.creatorId
                    }, {
                        limit: 1,
                        fields: { profile: 1 },
                        sort: { _id: 1 }
                    });
                }
            },
            /*
            {
                find: function (order) {
                    console.log(order)
                    this.unblock();
                    if (order.products) {
                        return Products.find({
                            _id: {
                                $in: order.products.map((x) => x._id)
                            }
                        }, {
                                fields: { name: 1 },
                                sort: { _id: 1 }
                            });
                    }
                }
            },
            */
            {
                find: function (order) {
                    this.unblock();
                    return Customers.find({
                        _id: order.customerId
                    }, {
                        limit: 1,
                        fields: { firstName: 1, lastName: 1 },
                        sort: { _id: 1 }
                    });
                }

            }
        ]
    };
});
