import { Orders } from './collection'
import { check } from 'meteor/check'

if (Meteor.isServer) {
    Meteor.publish('order', function (_id) {
        check(_id, String);
        return Orders.find(_id, {
            limit: 1
        })
    })
}