import { Orders } from './collection'
import { Customers } from '../customers'

Orders.helpers({
    customer() {
        const user = Customers.findOne(this.customerId)
        if (user) {
            return `${user.firstName} ${user.lastName}`;
        }
    },
    creator() {
        const user = Meteor.users.findOne(this.creatorId)
        if (user && user.profile) {
            return `${user.profile.firstName} ${user.profile.lastName}`;
        }
    }
});