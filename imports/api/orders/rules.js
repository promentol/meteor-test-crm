import { Orders } from './collection'

Orders.allow({
    insert(userId) {
        return !!userId
    },
    update(userId, doc) {
        return userId == doc.creatorId
    },
    remove(userId, doc) {
        return userId == doc.creatorId
    }
})