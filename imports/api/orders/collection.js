import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { Customers } from '../customers';
import { Products } from '../products';

const OrderSchema = new SimpleSchema({
    creatorId: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return { $setOnInsert: this.userId };
            } else {
                this.unset();
            }
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    customerId: {
        label: 'Customer',
        type: String,
        autoform: {
            type: 'select2',
            options: function () {
                console.log(
                    Customers.find().fetch().map((x) => {
                        return {
                            label: `${x.firstName} ${x.lastName}`,
                            value: x._id
                        }
                    })

                )
                return Customers.find().fetch().map((x) =>{
                    return {
                        label: `${x.firstName} ${x.lastName}`,
                        value: x._id
                    }
                })
            }
        }
    },
    products: {
        type: Array
    },
    "products.$": {
        type: Object
    },
    "products.$._id": {
        type: String,
        label: "Product Name",
        autoform: {
            type: 'select2',
            options: function () {
                return Products.find().fetch().map((x) => {
                    return {
                        label: x.name,
                        value: x._id
                    }
                })
            }
        }
    },
    "products.$.quantity": {
        type: SimpleSchema.Integer,
        min: 0
    },
    createdAt: {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return { $setOnInsert: new Date() };
            } else {
                this.unset();
            }
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    updatedAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    }
});

export const Orders = new Mongo.Collection('orders')

Orders.attachSchema(OrderSchema);