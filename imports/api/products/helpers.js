import { Products } from './collection'

Products.helpers({
    creator() {
        const user = Meteor.users.findOne(this.creatorId)
        if(user && user.profile) {
            return `${user.profile.firstName} ${user.profile.lastName}`;
        }
    }
});