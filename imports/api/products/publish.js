import { Products } from './collection'
import { check } from 'meteor/check'

if (Meteor.isServer) {
    Meteor.publish('product', function (_id) {
        check(_id, String);
        return Products.find(_id, {
            limit: 1
        })
    })

    Meteor.publish('productList', function () {
        return Products.find({}, {
            fields: {
                name: 1
            }
        })
    })
}