import { SimpleSchema } from 'meteor/aldeed:simple-schema'

const ProductSchema = new SimpleSchema({
    name: {
        type: String,
        max: 20
    },
    description: {
        type: String,
        max: 500,
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea",
                rows: 5,
            }
    	}
    },
    creatorId: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return { $setOnInsert: this.userId };
            } else {
                this.unset();
            }

        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    createdAt: {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return { $setOnInsert: new Date() };
            } else {
                this.unset();
            }
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    updatedAt: {
        type: Date,
        autoValue: function () {
            return new Date();
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    }
});

export const Products = new Mongo.Collection('products')

Products.attachSchema(ProductSchema);