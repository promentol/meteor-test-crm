import { Products } from './collection'

Products.allow({
    insert(userId) {
        return !!userId
    },
    update(userId, doc) {
        return userId == doc.creatorId
    },
    remove(userId, doc) {
        return userId == doc.creatorId
    }
})