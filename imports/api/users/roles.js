export const ROLES = {
    ADMIN: 'admin',
    MODERATOR: 'moderator',
    PRODUCTS: 'products',
    ORDERS: 'orders',
    CUSTOMERS: 'customers'
}