import _ from 'lodash'

Meteor.users.deny({
    insert() {
        return true;
    },
    update(userId, doc, fields) {
        return userId == doc._id && !_.isEqual(fields, ['profile'])
    },
    remove() {
        return true;
    }
})