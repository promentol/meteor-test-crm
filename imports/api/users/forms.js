import { UserProfile } from './collection'
import { ROLES } from './roles'

export const insertUserForm = new SimpleSchema({
    username: {
        type: String
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    profile: {
        type: UserProfile,
    },
    passwords: {
        type: Object
    },
    "passwords.password": {
        type: String,
        min: 6,
        max: 25,
        autoform: {
            label: "Password 6-25 in length",
            type: "password"
        }
    },
    "passwords.passswordConfirm": {
        type: String,
        label: "Password Confirmation",
        custom: function () {
            console.log(this.value, this.field('passwords.password').value, 'value')
            console.log(this.value == this.field('passwords.password').value)
            if (this.value != this.field('passwords.password').value) {
                console.log('passwordMissmatch')
                return "passwordMissmatch";
            }
        },
        autoform: {
            label: "Password 6-25 in length",
            type: "password"
        }
    },
    roles: {
        type: Array
    },
    'roles.$': {
        type: String,
        allowedValues: _.values(ROLES)
    },
})

export const updateUserForm = new SimpleSchema({
    profile: {
        type: UserProfile,
    },
    roles: {
        type: Array
    },
    'roles.$': {
        type: String,
        allowedValues: _.values(ROLES)
    }
})