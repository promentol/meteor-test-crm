import { check } from 'meteor/check'

if (Meteor.isServer) {
    Meteor.publish('user', function (_id) {
        check(_id, String)
        const params = {
            _id
        }
        console.log(this.userId, 'user id')
        if (!Roles.userIsInRole(this.userId, ['moderator'])) {
            throw new Meteor.Error("IS NOT MODERATOR")
        }
        if (!Roles.userIsInRole(this.userId, ['admin'])){
            params.role = {
                $ne: 'admin'
            }
        }
        console.log(params)
        return Meteor.users.find(params, {
            fields: {
                profile: 1,
                roles: 1
            }
        })
    })
}