Meteor.methods({
    updateUser(doc, _id) {
        //TODO validate
        check(doc, Object)
        if (!Roles.userIsInRole(this.userId, ['moderator'])) {
            throw new Meteor.Error("Moderator")
        }
        Meteor.users.update(_id, doc)
    },
    insertUser(doc, _id) {
        //TODO validate
        check(doc, Object)
        console.log(doc, {
            username: doc.username,
            email: doc.email,
            password: doc.passwords.password,
            profile: doc.profile,
        })
        const userId = Accounts.createUser({
            username: doc.username,
            email: doc.email,
            password: doc.passwords.password,
            profile: doc.profile,
        })

        Roles.addUsersToRoles(userId, doc.roles);

        return userId
    }
})