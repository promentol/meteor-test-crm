import { Customers } from './collection'

Customers.allow({
    insert(userId) {
        return !!userId
    },
    update(userId, doc) {
        return userId == doc.addedById
    },
    remove(userId, doc) {
        return userId == doc.addedById
    }
})