import { SimpleSchema } from 'meteor/aldeed:simple-schema'

const CustomerSchema = new SimpleSchema({
    firstName: {
        type: String,
        max: 20,
    },
    lastName: {
        type: String,
        max: 20
    },
    addresses: {
        type: Array
    },
    "addresses.$": {
        type: String,
        max: 100,
        autoform: {
            afFieldInput: {
                type: "textarea",
                rows: 5,
            }
        }
    },
    addedById: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return this.userId;
            } else if (this.isUpsert) {
                return { $setOnInsert: this.userId };
            } else {
                this.unset();
            }

        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    createdAt: {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return { $setOnInsert: new Date() };
            } else {
                this.unset();
            }
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    },
    updatedAt: {
        type: Date,
        optional: true,
        autoValue: function () {
            return new Date();
        },
        autoform: {
            afFieldInput: {
                type: "hidden"
            },
            afFormGroup: {
                label: false
            }
        }
    }
});

export const Customers = new Mongo.Collection('customers')
Customers.attachSchema(CustomerSchema);