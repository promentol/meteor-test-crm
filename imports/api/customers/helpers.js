import { Customers } from './collection'

Customers.helpers({
    creator() {
        const user = Meteor.users.findOne(this.addedById)
        if (user && user.profile) {
            return `${user.profile.firstName} ${user.profile.lastName}`;
        }
    }
})