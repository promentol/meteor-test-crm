import { Customers } from './collection'
import { check } from 'meteor/check'

if (Meteor.isServer) {
    Meteor.publish('customer', function (_id) {
        check(_id, String);
        return Customers.find(_id, {
            limit: 1
        })
    })

    Meteor.publish('customerList', function () {
        return Customers.find({}, {
            fields: {
                firstName: 1,
                lastName: 1
            }
        })
    })
}