import './navigation.html'
import { ReactiveVar } from 'meteor/reactive-var'

Template.navigation.rendered = function(){
    // Initialize metisMenu
    $('#side-menu').metisMenu();
};

Template.navigation.helpers({
    user: function () {
        return Meteor.user()
    }
})