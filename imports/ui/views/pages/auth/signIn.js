import './signIn.html'
import toastr from "toastr";

Template.signIn.rendered = function () {
}

Template.signIn.events({
    "submit form": function (event, template) {
        event.preventDefault()

        const target = event.target;
        const email = target.email.value;
        const password = target.password.value;

        Meteor.loginWithPassword(email, password, (err, res) => {
            if (err) {
                toastr.error(err.reason)
            } else {
                FlowRouter.redirect("/")
            }
        })
    }
})