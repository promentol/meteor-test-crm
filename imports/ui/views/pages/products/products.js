import './products.html'
import swal from 'sweetalert';
import { Products } from '../../../../api/products'

Template.products.rendered = function () {
    this.autorun(() => {
        if (!Roles.userIsInRole(Meteor.userId(), ['products'])) {
            FlowRouter.redirect('/')
        }
    })
}

Template.productActions.events({
    "click .editProduct": function () {
        FlowRouter.go(`/products/edit/${this._id}`)
    },
    "click .removeProduct": function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this product!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((value) => {
            if(value) {
                Products.remove(this._id, (err)=> {
                    if(err) {
                        swal("Error!", err.reason, "error");
                    } else {
                        swal("Good job!", "Product is removed!", "success");
                    }
                })
            } 
        });

    }
})
