import './productForm.html'
import { Products } from "../../../../api/products";
import toastr from "toastr";

AutoForm.addHooks('insertProductForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Product ${result} Added`)
        FlowRouter.go(`/products/edit/${result}`)
    }
});

AutoForm.addHooks('editProductForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Successfully Saved`)
    }
});

Template.productForm.created = function () {
    this.autorun(() => {
        FlowRouter.getParam('id') && this.subscribe('product', FlowRouter.getParam('id'));

        if (!Roles.userIsInRole(Meteor.userId(), ['products'])) {
            FlowRouter.redirect('/')
        }
    });
}

Template.productForm.helpers({
    Products: function () {
        return Products
    },
    product: function () {
        return Products.findOne()
    }
})