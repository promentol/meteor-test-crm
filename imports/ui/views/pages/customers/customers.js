import './customers.html'
import swal from 'sweetalert';
import { Customers } from '../../../../api/customers'

Template.customers.rendered = function () {
    this.autorun(()=>{
        if (!Roles.userIsInRole(Meteor.userId(), ['customers'])) {
            FlowRouter.redirect('/')
        }
    })
}

Template.customerActions.events({
    "click .editCustomers": function () {
        FlowRouter.go(`/customers/edit/${this._id}`)
    },
    "click .removeCustomer": function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this customer!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((value) => {
            if (value) {
                Customers.remove(this._id, (err) => {
                    if (err) {
                        swal("Error!", err.reason, "error");
                    } else {
                        swal("Good job!", "Customer is removed!", "success");
                    }
                })
            }
        });
    }
})