import './customerForm.html'
import { Customers } from '../../../../api/customers'
import toastr from "toastr";

AutoForm.addHooks('insertCustomerForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Customer ${result} Added`)
        FlowRouter.go(`/customers/edit/${result}`)
    }
});

AutoForm.addHooks('editCustomerForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Successfully Saved`)
    }
});

Template.customerForm.created = function () {
    this.autorun(() => {
        FlowRouter.getParam('id') && this.subscribe('customer', FlowRouter.getParam('id'));

        if (!Roles.userIsInRole(Meteor.userId(), ['customers'])) {
            FlowRouter.redirect('/')
        }
    });
}

Template.customerForm.helpers({
    Customers: function () {
        return Customers
    },
    customer: function () {
        return Customers.findOne()
    }
})