import './orderForm.html'
import { Orders } from '../../../../api/orders'
import toastr from "toastr";

AutoForm.addHooks('insertOrderForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Order ${result} Added`)
        FlowRouter.go(`/orders/edit/${result}`)
    }
});

AutoForm.addHooks('editOrderForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Successfully Saved`)
    }
});

Template.orderForm.created = function () {
    this.autorun(() => {
        FlowRouter.getParam('id') && this.subscribe('order', FlowRouter.getParam('id'));


        this.subscribe('customerList');
        this.subscribe('productList');

        if (!Roles.userIsInRole(Meteor.userId(), ['orders'])) {
            FlowRouter.redirect('/')
        }
    });
}

Template.orderForm.helpers({
    Orders: function () {
        return Orders
    },
    order: function () {
        return Orders.findOne()
    }
})