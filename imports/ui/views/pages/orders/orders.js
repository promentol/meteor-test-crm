import './orders.html'
import swal from 'sweetalert';
import { Orders } from '../../../../api/orders'

Template.orders.rendered = function () {
    this.autorun(() => {
        if (!Roles.userIsInRole(Meteor.userId(), ['orders'])) {
            FlowRouter.redirect('/')
        }
    })
}

Template.orderActions.events({
    "click .editOrder": function () {
        FlowRouter.go(`/orders/edit/${this._id}`)
    },
    "click .removeOrder": function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this order!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((value) => {
            if (value) {
                Orders.remove(this._id, (err) => {
                    if (err) {
                        swal("Error!", err.reason, "error");
                    } else {
                        swal("Good job!", "Order is removed!", "success");
                    }
                })
            }
        });
    }
})