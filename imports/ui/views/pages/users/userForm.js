import './userForm.html'
import toastr from "toastr";

import { UserProfile, insertUserForm, updateUserForm } from '../../../../api/users'

AutoForm.addHooks('insertUserForm', {
    onSuccess: function (formType, result) {
        toastr.success(`User ${result} Added`)
        FlowRouter.go(`/products/edit/${result}`)
    }
});

AutoForm.addHooks('editUserForm', {
    onSuccess: function (formType, result) {
        toastr.success(`Successfully Saved`)
    }
});

Template.userForm.created = function () {
    this.autorun(() => {
        FlowRouter.getParam('id') && this.subscribe('user', FlowRouter.getParam('id'));

        if (!Roles.userIsInRole(Meteor.userId(), ['moderator'])) {
            FlowRouter.redirect('/')
        }
    });
}

Template.userForm.helpers({
    insertUserForm: function() {
        return insertUserForm
    },
    updateUserForm: function () {
        return updateUserForm  
    },
    Users: function () {
        return Meteor.users
    },
    user: function () {
        return Meteor.users.findOne(FlowRouter.getParam('id'))
    }
})