import './users.html'

Template.users.rendered = function () {
    this.autorun(() => {
        if (!Roles.userIsInRole(Meteor.userId(), ['moderator'])) {
            FlowRouter.redirect('/')
        }
    })
}

Template.userActions.events({
    "click .editUser": function () {
        FlowRouter.go(`/users/edit/${this._id}`)
    },
    "click .removeProduct": function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((value) => {
            if (value) {
                Products.remove(this._id, (err) => {
                    if (err) {
                        swal("Error!", err.reason, "error");
                    } else {
                        swal("Good job!", "User is removed!", "success");
                    }
                })
            }
        });

    }
})
